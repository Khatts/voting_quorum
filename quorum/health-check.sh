#!/usr/bin/env bash
retries=12
is_running=0
is_api_running=0
while [[ retries -ge 0 && (is_running -eq 0 || is_api_running -eq 0 )]]
do
    if ((supervisorctl -c /etc/supervisord.conf status geth | grep -E RUNNING &> /dev/null)
      && (supervisorctl -c /etc/supervisord.conf status privacy_manager | grep -E RUNNING &> /dev/null))
    then
        is_running=1
    fi

    status_code=$(curl --write-out %{http_code} --silent --output /dev/null http://localhost:22000/upcheck)

    if ((status_code == 200 ))
    then
        is_api_running=1
    fi

    echo "Check for Quorum running = $is_running"
    echo "Check for Quorum API running = $is_api_running"
    if ((is_running == 1  && is_api_running == 1))
    then
        echo "Quorum is healthy"
        exit 0
    else
        echo "Sleeping for 10s"
        sleep 10
    fi
    echo "Retrying..."
    retries=$(($retries - 1))
done

echo "Healthcheck was unsuccessful :("
exit 1
