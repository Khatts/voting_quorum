#!/bin/bash

export QUORUM_DIR="/quorum"

set -e

# Only initialise the Quorum data directory if this has not been done before
if [[ ! -e "${QUORUM_DIR}/geth/genesis.json" && ! -d "${QUORUM_DIR}/chaindata" ]]; then
    echo "Copying genesis.json to geth path ${QUORUM_DIR}/geth/genesis.json"
    mkdir -p ${QUORUM_DIR}/geth
    cp /data/genesis.json ${QUORUM_DIR}/geth/genesis.json

    echo "Copying Constellation keys to Quorum data directory ${QUORUM_DIR}"
    echo ${CONSTELLATION_PUB} > ${QUORUM_DIR}/constellation.pub
    echo ${CONSTELLATION_KEY} > ${QUORUM_DIR}/constellation.key
    chmod 0600 ${QUORUM_DIR}/constellation.key

    echo "Initialising the Quorum node"
    /usr/local/bin/geth --datadir ${QUORUM_DIR} init ${QUORUM_DIR}/geth/genesis.json

    echo "Copying configured accountkey to Quorum data directory ${QUORUM_DIR}"
    echo ${ACCOUNTKEY} > ${QUORUM_DIR}/accountkey

    echo "Importing configured account into Quorum"
    echo "" > ${QUORUM_DIR}/password.txt
    /usr/local/bin/geth account import --datadir ${QUORUM_DIR} --password ${QUORUM_DIR}/password.txt ${QUORUM_DIR}/accountkey
    rm ${QUORUM_DIR}/password.txt

    echo "Copying configured nodekey to geth path ${QUORUM_DIR}/geth"
    echo ${NODEKEY} > ${QUORUM_DIR}/geth/nodekey
else
    echo "Quorum node already initialised. Reading state from provided directory ${QUORUM_DIR}"
fi


# Clean up old privacy manager socket file descriptor if it exists
if [ -e /data/privacy_manager.ipc ]; then
    rm /data/privacy_manager.ipc
fi

if [ ! -e ${QUORUM_DIR}/password.txt ]; then
    echo "" > ${QUORUM_DIR}/password.txt
fi

if [ "$PERMISSIONED_NODES" == "" ]; then
    echo -e "Please provide permissioned nodes as environment variables.\nExiting.............."
    exit 1
fi

if [ "$CONSTELLATION_NODES" == "" ]; then
    echo -e "Please provide constellation nodes as environment variables.\nExiting.............."
    exit 1
fi

for node in ${CONSTELLATION_NODES//,/ }
do
    PEERS+={\"url\":\"${node}\"},
done
sed -i "s@QUORUM_NODE_ADDRESS@${QUORUM_NODE_ADDRESS}@g" /data/tessera-config.json
sed -i "s@PEERS@${PEERS::-1}@g" /data/tessera-config.json
sed -i "s@TESSERA_KEY@${CONSTELLATION_KEY}@g" /data/tessera-config.json
sed -i "s@TESSERA_PUB@\"${CONSTELLATION_PUB}\"@g" /data/tessera-config.json
sed -i "s@https@http@g" /data/tessera-config.json

echo "${PERMISSIONED_NODES}" > ${QUORUM_DIR}/permissioned-nodes.json
cp ${QUORUM_DIR}/permissioned-nodes.json ${QUORUM_DIR}/static-nodes.json
echo "Updated ${QUORUM_DIR}/permissioned-nodes.json and ${QUORUM_DIR}/static-nodes.json"

chmod +x /data/start_geth.sh
mkdir -p /var/log/supervisor

/usr/bin/supervisord -c /etc/supervisord.conf -n
