#!/bin/bash

while ! [ -e  /data/privacy_manager.ipc ]; do
    echo 'Waiting for privacy manager to start...'
    sleep 1
done
PRIVATE_CONFIG=/data/privacy_manager.ipc nohup /usr/local/bin/geth \
		--unlock "0" \
		--password ${QUORUM_DIR}/password.txt \
    --datadir ${QUORUM_DIR} \
    --permissioned \
    --raft \
    --rpc \
    --rpcaddr 0.0.0.0 \
    --rpccorsdomain "*" \
    --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,raft \
    --emitcheckpoints \
    --raftport 50401 \
    --rpcport 22000 \
    --port 21000 \
    --verbosity 1 \
    --maxpeers 100 \
    --nodiscover \
    --gcmode archive \
    --rpcvhosts '*'