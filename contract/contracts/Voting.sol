pragma solidity >=0.4.21 <0.6.0;

contract Voting {
  mapping(string => uint) private votes;
  mapping(address => bool) private checklist;

  constructor() public {

  }

  function vote(string memory _candidate) public {
    if (checklist[msg.sender] != true) {
      votes[_candidate] = votes[_candidate] + 1;
      checklist[msg.sender] = true;
    }
  }

  function retrieve(string memory _candidate) public view returns (uint){
    return votes[_candidate];
  }

  function hasVoted() public view returns (bool) {
    return checklist[msg.sender];
  }
}
