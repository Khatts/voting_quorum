const Voting = artifacts.require("Voting");
const keys = ["p6m2ZLsWrwUZ40LfkwCI38abGd5SVFIwiKQnrbcOCD4=","vXyr0gidIQbE3ZpBThlqEKhIc2C48a5n62ftfD4ypUY=","1iTZde/ndBHvzhcl7V68x44Vx7pl8nwx9LqnM/AfJUg="];

module.exports = function(deployer) {
  deployer.deploy(Voting, {privateFor: keys});
};
