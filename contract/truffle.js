module.exports = {
  networks: {
    quorum: {
      host: '172.19.3.2',
      port: 22000,
      network_id: "*",
      gasPrice: 0,
      gas: 4500000
    }
  }
};
