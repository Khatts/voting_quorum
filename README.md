# Voting Application (Quorum)

## Pre-requisites
* Install docker on your machine
* (Optional) Qurum - https://github.com/jpmorganchase/quorum/releases 

## Setup
* Start the setup using ```docker-compose up -d``` (Image building takes some time)
* Check the containers using ```docker ps```
* There should be 4 containers running - 
    * voter_1
    * voter_2
    * commission
    * web
* The web application can be access on http://localhost:3000
* For entering the system use the following ids : 
    * Voter 1 - voter1
    * Voter 2 - voter2
    * Commission - commission
* Please note that these keys are nothing else but privacy manager keys which quroum uses. These are public keys hence there is no security flaw in exposing them.


## Debugging
* The quorum nodes are exposed at 22001, 22002 and 22003 ports on localhost
* Connect to any node using : ```geth attach http://localhost:<port>```

## Commands
* Start setup - ```docker-compose up -d ```
* Rebuild Setup - ```docker-compose up -d --build```
* Clean and rebuild setup - ```rm -rf work && docker-compose up -d --build```
* Check status of containers - ```docker ps```

#### Quorum
* Attach to geth console - ```geth attach http://localhost:<port>```
* Get block - ```eth.getBlock(<blocknumber>)```
* Get transaction of a block - ```eth.getTransaction(eth.getBlock(<blocknumber>)).transactions[0]```
* Get transaction receipt - ```eth.getTransactionReceipt(eth.getBlock(<blocknumber>)).transactions[0]```


