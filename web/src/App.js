import React from 'react';
import Voting from './components/Voting/Voting';

function App() {
  return (
    <div className="App w-100 h-100 bg-dark position-absolute">
      <Voting/>
    </div>
  );
}

export default App;
