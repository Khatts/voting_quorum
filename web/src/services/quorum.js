import Web3 from 'web3';
import VotingContract from '../contracts/Voting';

export default class Quorum {

    constructor(rpcUrl) {
        this.web3 = new Web3(rpcUrl);
        this.votingContract = new this.web3.eth.Contract(VotingContract.abi, VotingContract.networks["1337"].address);
    }

    async isListening() {
        try {
            await this.web3.eth.net.isListening();
            console.log('Log : Successfully connected to quorum node');
        } catch (e) {
            console.log('Error : Error connecting to quorum node');
        }
    }

    async getBlock(blocknumber) {
        return await this.web3.eth.getBlock(blocknumber);
    }

    async vote(candidate, accountAddressOfNode, privateFor) {
        console.log('Log : Initiated a transaction');
        const receipt = await this.votingContract.methods.vote(candidate).send({
            from: accountAddressOfNode,
            privateFor: privateFor
        });
        console.log('Log : Transaction completed');
        console.log('Log : Fetching transaction details');
        const transaction = await this.web3.eth.getTransaction(receipt.transactionHash);
        console.log('Log : Fetching transaction details completed');
        console.log('Transaction : \n', transaction);
        console.log('Transaction Receipt : \n', receipt);
        console.log('The transaction was performed by : \n', transaction.from);
        console.log('The transaction was sent to : \n', transaction.to);
    }

    async hasVoted(accountAddressOfNode) {
        console.log('Log : Checking if voter has casted');
        return await this.votingContract.methods.hasVoted().call({
            from: accountAddressOfNode
        });
    }

    async getVotes(candidates) {
        console.log('Log : Getting votes');
        const results = [];
        for (const key in candidates) {
            const candidate = candidates[key];
            const votes = await this.getVote(candidate);
            results.push({
                ...candidate,
                votes: votes
            })
        }
        console.log('Log : Getting votes completed');
        return results || [];
    }

    async getVote(candidate) {
        return await this.votingContract.methods.retrieve(candidate.id).call();
    }
}
