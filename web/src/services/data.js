import nodes from "../common/data/nodes";
import candidates from "../common/data/candidates";

function isNodePresentForId(id) {
    return nodes.filter(node => node.id === id).length === 1;
}

function getNodeInfoForPrivacyManagerKey(id) {
    const filteredNodes = nodes.filter(node => node.id === id);
    if (filteredNodes.length === 1) {
        return filteredNodes[0];
    }
}

function getCandidates() {
    return candidates;
}

function isNodeCommission(id) {
    return nodes.filter(node => node.id === id && node.type === 'COMMISSION').length === 1;
}

function getPrivacyManagerKeyForCommission() {
    const filteredNodes = nodes.filter(node => node.type === 'COMMISSION');
    if (filteredNodes.length === 1) {
        return filteredNodes[0].constellationPub;
    }
}

export {
    isNodePresentForId,
    getNodeInfoForPrivacyManagerKey,
    getCandidates,
    getPrivacyManagerKeyForCommission,
    isNodeCommission
}
