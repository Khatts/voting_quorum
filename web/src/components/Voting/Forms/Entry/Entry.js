import React from "react"
import {Row, Col, Label, FormGroup, Input, Button} from 'reactstrap';

class Entry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            entryKey: ''
        };

        this.handleEntryKeyChange = this.handleEntryKeyChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleEntryKeyChange(event) {
        this.setState({entryKey: event.target.value});
    }

    handleSubmit(event) {
        this.props.handleEntry(this.state);
    }

    render() {
        return (
            <React.Fragment>
                <Row className="m-5">
                    <Col lg={{size: 6, offset: 3}} md={{size: 8, offset: 2}} sm={{size: 10, offset: 1}} xs={{size: 12}}>
                        <Label className="font-weight-bold text-white">Please enter your id</Label>
                        <FormGroup>
                            <Input
                                id="dashboard-name"
                                type="text"
                                className="form-control"
                                value={this.state.entryKey}
                                onChange={this.handleEntryKeyChange}
                                required
                                maxLength="100"
                                minLength="3"/>
                            <Label className="text-danger">{this.props.error}</Label>
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" size="md" onClick={(event) => this.handleSubmit(event)}>
                                Submit
                            </Button>
                        </FormGroup>

                    </Col>
                </Row>
            </React.Fragment>
        )
    }
};

export default Entry;
