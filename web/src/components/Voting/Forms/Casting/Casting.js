import React from "react"
import {Row, Col, Label, FormGroup, Input, Button} from 'reactstrap';

class Casting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            candidate: '',
            error: ''
        };

        this.handleCandidateSelectionChange = this.handleCandidateSelectionChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCandidateSelectionChange(event) {
        this.setState({candidate: event.target.value});
    }

    handleSubmit() {
        if (this.state.candidate === '') {
            this.setState({
                error: 'Please select a candidate'
            });
            return;
        }
        this.setState({
            error: ''
        });
        this.props.handleCasting(this.state.candidate);
    }

    getCandidateOptions(candidates) {
        return candidates.map(candidate => <option key={candidate.id} value={candidate.id}>{candidate.name}</option>)
    }

    render() {
        return (
            <React.Fragment>
                <Row>
                    <Col lg={{size: 8, offset: 1}} md={{size: 10, offset: 1}} sm={{size: 10, offset: 1}} xs={{size: 12}}>
                        <Label className="font-weight-bold text-white">Please select a candidate to vote for</Label>
                        <FormGroup>
                            <Input type="select"
                                   name="select"
                                   id="candidates"
                                   value={this.state.candidate}
                                   onChange={(event) => this.handleCandidateSelectionChange(event)}>
                                <option value="" disabled defaultValue>Select your option</option>
                                {this.getCandidateOptions(this.props.candidates)}
                            </Input>
                            <Label className="text-danger">{this.state.error || this.props.error}</Label>
                            <Label className="text-success">{this.props.success}</Label>
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" size="md" onClick={() => this.handleSubmit()}>
                                Vote
                            </Button>
                        </FormGroup>

                    </Col>
                </Row>
            </React.Fragment>
        )
    }
};

export default Casting;
