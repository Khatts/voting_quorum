import React from 'react';
import {Col, Label, Table} from 'reactstrap';

const Result = (props) => {
    function getVoteRows(votes) {
        return votes.map(vote => {
            return (
                <tr key={vote.id}>
                    <td>{vote.name}</td>
                    <td>{vote.votes}</td>
                </tr>
            )
        })
    }

    return (
        <React.Fragment>
            <Label className="font-weight-bold text-white">Please enter your privacy manager key to continue</Label>
            <Table responsive className="text-white">
                <thead>
                <tr>
                    <th>Candidate</th>
                    <th>Votes</th>
                </tr>
                </thead>
                <tbody>
                {getVoteRows(props.votes)}
                </tbody>
            </Table>
        </React.Fragment>
    )
};

export default Result;
