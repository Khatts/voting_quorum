import React from "react"
import Entry from './Forms/Entry/Entry';
import Quorum from '../../services/quorum';
import Casting from './Forms/Casting/Casting';
import Result from './Result/Result';
import {Row, Col} from 'reactstrap';
import {
    getNodeInfoForPrivacyManagerKey,
    isNodePresentForId,
    getCandidates,
    getPrivacyManagerKeyForCommission,
    isNodeCommission
} from '../../services/data';

class Voting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            quorum: {
                isConnected: false,
                constellationPub: '',
                rpc: undefined
            },
            error: {
                entry: '',
                casting: ''
            },
            success: {
                casting: ''
            },
            votes: []
        }
    }

    async handleEntry(args) {
        if (!isNodePresentForId(args.entryKey)) {
            this.setState({
                error: {
                    entry: 'There is no node found with the following key'
                }
            });
            return;
        }
        const node = getNodeInfoForPrivacyManagerKey(args.entryKey);
        const rpc = new Quorum(node.rpcUrl);
        await rpc.isListening();
        const votes = await rpc.getVotes(getCandidates());
        this.setState({
            quorum: {
                constellationPub: args.entryKey,
                isConnected: true,
                rpc: rpc
            },
            error: {
                entry: ''
            },
            votes: votes
        });
    }

    async handleCasting(candidateId) {
        if (this.state.constellationPub !== '') {
            const nodeInfo = getNodeInfoForPrivacyManagerKey(this.state.quorum.constellationPub);
            const hasVoted = await this.state.quorum.rpc.hasVoted(nodeInfo.accountAddress);
            if (hasVoted) {
                this.setState({
                    error: {
                        casting: 'Sorry, you have already cast a vote.'
                    },
                    success: {
                        casting: ''
                    }
                });
                return;
            }

            const  privacyManagerKeyForCommission = getPrivacyManagerKeyForCommission();
            await this.state.quorum.rpc.vote(candidateId, nodeInfo.accountAddress, [privacyManagerKeyForCommission]);
            const votes = await this.state.quorum.rpc.getVotes(getCandidates());
            this.setState({
                votes: votes,
                success: {
                    casting: 'Your vote has been successfully casted'
                }
            })
        }
    }

    getEntry(isConnected) {
        if (!isConnected) {
            return <Entry handleEntry={(args) => this.handleEntry(args)} error={this.state.error.entry}/>
        }
    }

    getCasting(constellationPub, castingError, castingSuccess) {
        if (!isNodeCommission(constellationPub)) {
            return (
                <Col lg={{size: 6, offset: 3}} md={{size: 8, offset: 2}} sm={{size: 10, offset: 1}} xs={{size: 12}}>
                    <Casting
                        candidates={getCandidates()}
                        error={castingError}
                        success={castingSuccess}
                        handleCasting={(candidateId) => this.handleCasting(candidateId)}
                    />
                </Col>
            );
        }
    }

    getResults(constellationPub) {
        if (isNodeCommission(constellationPub)) {
            return (
                <Col lg={{size: 6, offset: 3}} md={{size: 8, offset: 2}} sm={{size: 10, offset: 1}} xs={{size: 12}}>
                    <Result votes={this.state.votes}/>
                </Col>
            );
        }
    }

    getDashboard(isConnected, constellationPub, castingError, castingSuccess) {
        if (isConnected) {
            return (
                <Row className="m-5">
                    {this.getCasting(constellationPub, castingError, castingSuccess)}
                    {this.getResults(constellationPub)}

                </Row>
            )
        }
    }

    render() {
        return (
            <React.Fragment>
                {this.getEntry(this.state.quorum.isConnected)}
                {this.getDashboard(
                    this.state.quorum.isConnected,
                    this.state.quorum.constellationPub,
                    this.state.error.casting,
                    this.state.success.casting
                )}
            </React.Fragment>
        )
    }
};

export default Voting;
